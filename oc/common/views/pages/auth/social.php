<style>
    .btn_facebook,.btn_google{color:#fff;width:150px;padding:8px 12px;float:left;border-radius:4px;}
    .btn_facebook{background:#46629e;}
    .btn_facebook:hover{color:#fff;text-decoration:none;background:#395185;}
    .btn_google{background:#d94634;}
    .btn_google:hover{color:#fff;text-decoration:none;background:#BB3D2E;}
    
</style>

<fieldset>
        <?$providers = Social::get_providers(); $i = 0; foreach ($providers as $key => $value):?>
            <?if($value['enabled']):?>
                <?if(strtolower($key) == 'live')$key='windows'?>
                <div class="col-xs-12">
                    <?php
                    if($key == 'Facebook'){
                    ?>
                        
                        <a class="zocial <?=strtolower($key)?> social-btn btn_facebook" href="<?=Route::url('default',array('controller'=>'social','action'=>'login','id'=>strtolower($key)))?>">
                            <i class="fa fa-3 fa-facebook" style="margin-right:10px;"></i>
                                <?=$key?>
                        </a>
                    <?php
                    }
                    
                    if($key == 'Google'){
                    ?>    
                        
                        <a class="zocial <?=strtolower($key)?> social-btn btn_google" href="<?=Route::url('default',array('controller'=>'social','action'=>'login','id'=>strtolower($key)))?>">
                            <i class="fa fa-3 fa-google" style="margin-right:10px;"></i>
                                <?=$key?>
                        </a>
                    <?php
                    }
                    ?>               
                </div>
            <?$i++; endif?>
        <?endforeach?>
</fieldset>