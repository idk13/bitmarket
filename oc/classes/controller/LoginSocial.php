<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Classe que gerencia a autenticação através das redes sociais e servidores de email
 *
 * @author Alan
 */
class Controller_LoginSocial extends Controller
{
    
    protected $appId;
    protected $appSecret;
    protected $scopeFB = [];
    protected $fb;
            
    /**
     * Dados de config do app login
     */
    private function initFB()
    {
        // pega os dados no banco com as configurações no painel admin
        $social = json_decode(core::config('social.config'));
        $this->appId =  $social->providers->Facebook->keys->id;
        $this->appSecret =  $social->providers->Facebook->keys->secret;
        
        // dados a ser solicitados
        $this->scopeFB = ['email','name','picture','gender','address'];
        
    }
    
    /**
     * Verifica e toma ação de register ou login do usuário
     */
    public function action_fb()
    {   
       // carrega os dados do APP 
       $this->initFB();
       
       // ação do usuário [login ou register]
       $action = filter_input(INPUT_GET, 'action');
       
       // uri de redirecionamento
       $redirectUri = urlencode(Route::url('fb-auth') . '?action=' . $action);
       
       // code da query passado pelo FB
       $code = filter_input(INPUT_GET, 'code');
       
       // criação do token
       $urlToken = 'https://graph.facebook.com/oauth/access_token?client_id=' . $this->appId . '&redirect_uri=' .  $redirectUri . '&client_secret=' . $this->appSecret . '&code=' . $code;      
       
       // Requisição com a url de token de acesso
       $response = @file_get_contents($urlToken);
       
       if(!!$response){
           
            $params = null;
            parse_str($response, $params);
           
            // dados através do token de acesso
            $graphUrl = 'https://graph.facebook.com/me?access_token=' . $params['access_token'] . '&fields=' . implode(',',$this->scopeFB);
            $user = json_decode(file_get_contents($graphUrl));
               
            if(!!$user->email) {
                
                return !!$action && $action === 'login' ? $this->login_fb($user) : $this->register($user); 
                
            }
            
            // informa-lo que ele necessita de permitir o acesso ao email e perfil público
            // code do feedback ...
            return;
            
        }
        
        // redirecionar para uma mensagem de erro
       
       
    }
    
    /**
     * Loga o user no sistema
     * @param type $user = dados do user no facebook
     */
    public function login_fb($user)
    {
        
    }
    
    /**
     * Registra e loga o user no sistema
     * @param type $user = dados do user no facebook
     */
    public function register($user)
    {
        
    }
    
}
