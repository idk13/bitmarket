<?php defined('SYSPATH') or die('No direct script access.');
return array
(
    'default' => array(
        'type'       => 'mysqli',
        'connection' => array(
            'hostname'   => '46.101.52.28',
            'username'   => 'bitmark',
            'password'   => 'bitmark',
            'persistent' => FALSE,
            'database'   => 'gigantee',
            ),
        'table_prefix' => 'oc2_',
        'charset'      => 'utf8',
        'profiling'    => (Kohana::$environment===Kohana::DEVELOPMENT)? TRUE:FALSE,
     ),
);
