<?php defined('SYSPATH') or die('No direct script access.');?>
<hr>
<footer>
    <div class="row">
        <?$i=0; foreach ( Widgets::render('footer') as $widget):?>
            <div class="col-md-3">
                <?=$widget?>
            </div>
            <? $i++; if ($i%4 == 0) echo '<div class="clearfix"></div>';?>
        <?endforeach?>
    </div>
    <!--This is the license for Open Classifieds, do not remove -->
    <div class="center-block box_footer">
        <!-- ADSENSE FOOTER -->
        <ins class="adsbygoogle"
            style="display:inline-block;width:728px;height:90px"
            data-ad-client="ca-pub-1090081997130719"
            data-ad-slot="5049689785"></ins>
       <script>
       (adsbygoogle = window.adsbygoogle || []).push({});
       </script>
       <!-- END ADSENSE FOOTER -->
        
     <!--   <p>&copy;
            Web Powered by <?=core::config('general.site_name')?> <?=date('Y')?>
            <?if(Core::config('appearance.theme_mobile')!=''):?>
                - <a href="<?=Route::url('default')?>?theme=<?=Core::config('appearance.theme_mobile')?>"><?=__('Mobile Version')?></a>
            <?endif?>
        </p>
    -->
    </div>
</footer>