<?php defined('SYSPATH') or die('No direct script access.');?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="<?=i18n::html_lang()?>"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="<?=i18n::html_lang()?>"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="<?=i18n::html_lang()?>"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="<?=i18n::html_lang()?>"> <!--<![endif]-->
<head>
<?=View::factory('header_metas',array('title'             => $title,
                                      'meta_keywords'     => $meta_keywords,
                                      'meta_description'  => $meta_description,
                                      'meta_copyright'    => $meta_copyright,))?> 
    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 7]><link rel="stylesheet" href="//blueimp.github.com/cdn/css/bootstrap-ie6.min.css"><![endif]-->
    <!--[if lt IE 9]>
      <script type="text/javascript" src="//cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <![endif]-->
    <?=Theme::styles($styles)?> 
    <?=Theme::scripts($scripts)?>
    <?=core::config('general.html_head')?>
    <?=View::factory('analytics')?>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

  <body data-spy="scroll" data-target=".subnav" data-offset="50">

    <?=View::factory('alert_terms')?>

    <?=$header?>
    <div class="container">
        <div class="alert alert-warning off-line" style="display:none;"><strong><?=__('Warning')?>!</strong> <?=__('We detected you are currently off-line, please connect to gain full experience.')?></div>
        <div class="row">
            <?if(Controller::$full_width):?>
                <div class="col-xs-12">
                    <?=Breadcrumbs::render('breadcrumbs')?>
                    <?=Alert::show()?>
                    <?=$content?>
                </div>
            <?else:?>
            <div class="col-xs-12 col-sm-2 boxs_sidebar" >
                <?=View::fragment('sidebar_front','sidebar')?>
                <div class="panel-body">
                    <!-- Banner Esquerda -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-1090081997130719"
                         data-ad-slot="7107858989"
                         data-ad-format="auto"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
                
            </div>
            <div class="col-xs-12 col-sm-8">
                <?=Breadcrumbs::render('breadcrumbs')?>
                <?=Alert::show()?>
                <?=$content?>
            </div>
            <!-- BANNER SIDE -->
            <div class="col-xs-12 col-sm-2 boxs_sidebar_right">                
                <ins class="adsbygoogle"
                    style="display:block"
                    data-ad-client="ca-pub-1090081997130719"
                    data-ad-slot="5041186583"
                    data-ad-format="auto"></ins>
               <script>
               (adsbygoogle = window.adsbygoogle || []).push({});
               </script>
               <div style="margin:20px 0;"></div>
               <ins class="adsbygoogle"
                    style="display:block"
                    data-ad-client="ca-pub-1090081997130719"
                    data-ad-slot="3285251786"
                    data-ad-format="auto"></ins>
               <script>
               (adsbygoogle = window.adsbygoogle || []).push({});
               </script>
               
            </div>
            <!-- END BANNER SIDE -->
            <?endif?>
        </div>
        <?=$footer?>
    </div>
  
    <?=Theme::scripts($scripts,'footer')?>
     <!-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>   --->   
    <?=core::config('general.html_footer')?>

  <?=(Kohana::$environment === Kohana::DEVELOPMENT)? View::factory('profiler'):''?>
  </body>
</html>
