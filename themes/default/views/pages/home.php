<?php defined('SYSPATH') or die('No direct script access.');?>
<?if(core::config('advertisement.ads_in_home') != 3):?>
    <div class="well box_home">
        <?if(core::config('advertisement.ads_in_home') == 0):?>
            <h3><?=__('Latest Ads')?></h3>
        <?elseif(core::config('advertisement.ads_in_home') == 1 OR core::config('advertisement.ads_in_home') == 4):?>
            <h3><?=__('Featured Ads')?></h3>
        <?elseif(core::config('advertisement.ads_in_home') == 2):?>
            <h3><?=__('Popular Ads last month')?></h3>
        <?endif?>
        <div class="row">
            <?$i=0; foreach($ads as $ad):?>
                <div class="col-md-3">
                    <div class="thumbnail latest_ads box_latest_ads">
                        <a href="<?=Route::url('ad', array('category'=>$ad->category->seoname,'seotitle'=>$ad->seotitle))?>"  class="min-h">
                            <?if($ad->get_first_image()!== NULL):?>
                                <img src="<?=$ad->get_first_image()?>" alt="<?=HTML::chars($ad->title)?>">
                            <?else:?>
                                <?if(( $icon_src = $ad->category->get_icon() )!==FALSE ):?>
                                    <img src="<?=$icon_src?>" alt="<?=HTML::chars($ad->title)?>" >
                                <?elseif(( $icon_src = $ad->location->get_icon() )!==FALSE ):?>
                                    <img src="<?=$icon_src?>" alt="<?=HTML::chars($ad->title)?>" >
                                <?else:?>
                                    <img src="//www.placehold.it/200x200&text=<?=HTML::entities($ad->category->name)?>" alt="<?=HTML::chars($ad->title)?>"> 
                                <?endif?> 
                            <?endif?>
                        </a>
                        <div class="caption title_box_ads">
                            <?php
                                $countChar = strlen($ad->title);
                                $titleAds = $countChar > 44 ? substr($ad->title, 0, 45).' ...' : $ad->title;
                                echo $titleAds;
                            ?>
                        </div> 
                        <span class="location_box_ads">
                            <i class="glyphicon glyphicon-map-marker"></i>
                            <?= ucfirst($ad->location->seoname); ?>
                        </span>                       
                        <span class="price_box_ads"><?= html_entity_decode(i18n::money_format($ad->price)) ?></span>
                        
                    </div>
                </div>     
            <?endforeach?>
        </div>
    </div>
<?endif?>
<div class='well box_home'>
    <h3><?=__("Categories")?></h3>
    <div class="row">
        <?$i=0; foreach($categs as $c):?>
            <?if($c['id_category_parent'] == 1 && $c['id_category'] != 1):?>
                <div class="col-md-4">
                    <div class="panel panel-home-categories">
                        <div class="panel-heading" style="font-size:14px;">
                            <a title="<?=HTML::chars($c['name'])?>" href="<?=Route::url('list', array('category'=>$c['seoname']))?>"><?=mb_strtoupper($c['name']);?></a>
                        </div>
                        <div class="panel-body style_menu">
                            <ul class="list-group">
                            	<?php $cont=1;?>
                                <?foreach($categs as $chi):?>
                                    <?if($chi['id_category_parent'] == $c['id_category'] && $cont <= '4'):?>
                                        <?
                                        $styleLacking = '';
                                        if($chi['name'] == 'Em falta'){
                                            $styleLacking = 'style="color:#e85647;font-weight:bold"';
                                        }?>
                                        <li class="list-group-item">
                                            <a title="<?=HTML::chars($chi['name'])?>" href="<?=Route::url('list', array('category'=>$chi['seoname']))?>" <?= $styleLacking; ?>><?=$chi['name'];?> <span class="pull-right badge badge-success"><?=$chi['count']?></span></a>
                                        </li>
                                        <? if($cont == '4'): ?>
                                    		<li><a title="<?=HTML::chars($c['name'])?>" href="<?=Route::url('list', array('category'=>$c['seoname']))?>"><?= __('More...'); ?></a></li>
                                    	<? endif; ?>
                                        <?php ++$cont; ?>
                                    <?endif?>
                                 <?endforeach?>
                            </ul>
                        </div>
                    </div>
                </div>
                <? $i++; if ($i%3 == 0) echo '<div class="clear"></div>';?>
            <?endif?>
        <?endforeach?>
    </div>
</div>
<?if(core::config('general.auto_locate')):?>
    <input type="hidden" name="auto_locate" value="<?=core::config('general.auto_locate')?>">
    <?if(count($auto_locats) > 0):?>
        <div class="modal fade" id="auto-locations" tabindex="-1" role="dialog" aria-labelledby="autoLocations" aria-hidden="true">
            <div class="modal-dialog	modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 id="autoLocations" class="modal-title text-center"><?=__('Please choose your closest location')?></h4>
                    </div>
                    <div class="modal-body">
                        <div class="list-group">
                            <?foreach($auto_locats as $loc):?>
                                <a href="<?=Route::url('list', array('location'=>$loc->seoname))?>" class="list-group-item"><span class="pull-right"><span class="glyphicon glyphicon-chevron-right"></span></span> <?=$loc->name?> (<?=i18n::format_measurement($loc->distance)?>)</a>
                            <?endforeach?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?endif?>
<?endif?>