<?php defined('SYSPATH') or die('No direct script access.');?>

<?if(count($ads)):?>
    <div class="options_listing pull-right">
        <div class="type_view_listing pull-right">
<!--            <span data-view="grid" class="mode_view_listing glyphicon glyphicon-th-large" title="Grade"></span> 
            <span data-view="list" class="mode_view_listing glyphicon glyphicon-list" title="Lista"></span>-->
        </div>
    </div>
    <div class="page-header">
        <h3><?=__('Related ads')?></h3>
    </div>
    <?php
        // alternar o modo de exibição dos anuncios
        $cookie_view_list = filter_input(INPUT_COOKIE, 'view_list');

        // verifico o modo de visuazalição e dependendo do valor, adiciono um estilo diferente através de classes bootstrap
        switch($cookie_view_list){
            case 'grid':
                $view_list = 'col-xs-12 col-sm-12 col-md-6';
                break;
            case 'list':
                $view_list = 'list';
                break;
            default:
                $view_list = 'col-xs-12 col-sm-12 col-md-6';
        }
    ?>
    <?$i=0; foreach($ads as $ad):?>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="thumbnail latest_ads box_latest_ads">
                <a href="<?= Route::url('ad', array('category' => $ad->category->seoname, 'seotitle' => $ad->seotitle)) ?>"  class="min-h">
                    <?if($ad->get_first_image()!== NULL):?>
                    <img src="<?= $ad->get_first_image() ?>" alt="<?= HTML::chars($ad->title) ?>">
                    <?else:?>
                    <?if(( $icon_src = $ad->category->get_icon() )!==FALSE ):?>
                    <img src="<?= $icon_src ?>" alt="<?= HTML::chars($ad->title) ?>" >
                    <?elseif(( $icon_src = $ad->location->get_icon() )!==FALSE ):?>
                    <img src="<?= $icon_src ?>" alt="<?= HTML::chars($ad->title) ?>" >
                    <?else:?>
                    <img src="//www.placehold.it/200x200&text=<?= HTML::entities($ad->category->name) ?>" alt="<?= HTML::chars($ad->title) ?>"> 
                    <?endif?> 
                    <?endif?>
                </a>
                <div class="caption title_box_ads">
                    <?php
                    $countChar = strlen($ad->title);
                    $titleAds = $countChar > 44 ? substr($ad->title, 0, 45) . ' ...' : $ad->title;
                    echo $titleAds;
                    ?>
                </div> 
                <span class="location_box_ads">
                    <i class="glyphicon glyphicon-map-marker"></i>
                    <?= ucfirst($ad->location->seoname); ?>
                </span>                       
                <span class="price_box_ads"><?= html_entity_decode(i18n::money_format($ad->price)) ?></span>

            </div>
        </div>     
    <?endforeach?>
<?endif?>
