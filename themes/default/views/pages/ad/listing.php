<?php defined('SYSPATH') or die('No direct script access.');?>

<div class="options_listing pull-right">
    
    <div class="type_view_listing pull-left">
       <!-- <span data-view="grid" class="mode_view_listing glyphicon glyphicon-th-large" title="Grade"></span> 
        <span data-view="list" class="mode_view_listing glyphicon glyphicon-list" title="Lista"></span>-->
    </div>
    
    <div class="btn-group pull-right">
        <?if (core::config('advertisement.map')==1):?>
            <a href="<?=Route::url('map')?>?category=<?=Model_Category::current()->loaded()?Model_Category::current()->seoname:NULL?>&location=<?=Model_Location::current()->loaded()?Model_Location::current()->seoname:NULL?>" 
                class="btn btn-default btn-sm <?=(core::cookie('list/grid')==0)?'active':''?>">
                <span class="glyphicon glyphicon-globe"></span> <?=__('Map')?>
            </a>
        <?endif?>
        <button style="margin-bottom: 10px;" type="button" id="sort" data-sort="<?=core::request('sort')?>" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">
            <span class="glyphicon glyphicon-list-alt"></span> <?=__('Sort')?> <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" id="sort-list">
            <li><a href="<?=Request::current()->url()?>?sort=title-asc"><?=__('Name (A-Z)')?></a></li>
            <li><a href="<?=Request::current()->url()?>?sort=title-desc"><?=__('Name (Z-A)')?></a></li>
            <?if(core::config('advertisement.price')!=FALSE):?>
                <li><a href="<?=Request::current()->url()?>?sort=price-asc"><?=__('Price (Low)')?></a></li>
                <li><a href="<?=Request::current()->url()?>?sort=price-desc"><?=__('Price (High)')?></a></li>
            <?endif?>
            <li><a href="<?=Request::current()->url()?>?sort=featured"><?=__('Featured')?></a></li>
            <?if(core::config('general.auto_locate')):?>
                <li><a href="<?=Request::current()->url()?>?sort=distance" id="sort-distance"><?=__('Distance')?></a></li>
            <?endif?>
            <li><a href="<?=Request::current()->url()?>?sort=published-desc"><?=__('Newest')?></a></li>
            <li><a href="<?=Request::current()->url()?>?sort=published-asc"><?=__('Oldest')?></a></li>
        </ul>
    </div>
    
</div>

<div class="page-header">
    <?if ($category!==NULL):?>
        <h1><?=$category->name?></h1>
    <?elseif ($location!==NULL):?>
        <h1><?=$location->name?></h1>
    <?else:?>
        <h1><?=__('Listings')?></h1>
    <?endif?>
</div>

<!-- div class="well" id="recomentadion">
    <?if (Controller::$image!==NULL):?>
        <img src="<?=Controller::$image?>" class="img-responsive" alt="<?=($category!==NULL) ? HTML::chars($category->name) : (($location!==NULL AND $category===NULL) ? HTML::chars($location->name) : NULL)?>">
    <?endif?>

    <p>
        <?if ($category!==NULL):?>
            <?=$category->description?> 
        <?elseif ($location!==NULL):?>
            <?=$location->description?>
        <?endif?>
    </p>
    
    <?if (Core::config('advertisement.only_admin_post')=='desativado'):?>
        <i class="glyphicon glyphicon-pencil"></i> 
        <a title="<?=__('New Advertisement')?>" 
            href="<?=Route::url('post_new')?>?category=<?=($category!==NULL)?$category->seoname:''?>&location=<?=($location!==NULL)?$location->seoname:''?>">
            <?=__('Publish new advertisement')?>
        </a>
    <?endif?>
</div><!--end of recomentadion-->


<?php
    // alternar o modo de exibição dos anuncios
    $cookie_view_list = filter_input(INPUT_COOKIE, 'view_list');
    
    // verifico o modo de visuazalição e dependendo do valor, adiciono um estilo diferente através de classes bootstrap
    switch($cookie_view_list){
        case 'grid':
            $view_list = 'col-xs-12 col-sm-6 col-md-3';
            break;
        case 'list':
            $view_list = 'list';
            break;
        default:
            $view_list = 'col-xs-12 col-sm-6 col-md-3';
    }
?>
<?if(count($ads)):?>
    <?$i=0; foreach($ads as $ad):?>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="thumbnail latest_ads box_latest_ads">
                <a href="<?= Route::url('ad', array('category' => $ad->category->seoname, 'seotitle' => $ad->seotitle)) ?>"  class="min-h">
                    <?if($ad->get_first_image()!== NULL):?>
                    <img src="<?= $ad->get_first_image() ?>" alt="<?= HTML::chars($ad->title) ?>">
                    <?else:?>
                    <?if(( $icon_src = $ad->category->get_icon() )!==FALSE ):?>
                    <img src="<?= $icon_src ?>" alt="<?= HTML::chars($ad->title) ?>" >
                    <?elseif(( $icon_src = $ad->location->get_icon() )!==FALSE ):?>
                    <img src="<?= $icon_src ?>" alt="<?= HTML::chars($ad->title) ?>" >
                    <?else:?>
                    <img src="//www.placehold.it/200x200&text=<?= HTML::entities($ad->category->name) ?>" alt="<?= HTML::chars($ad->title) ?>"> 
                    <?endif?> 
                    <?endif?>
                </a>
                <div class="caption title_box_ads">
                    <?php
                    $countChar = strlen($ad->title);
                    $titleAds = $countChar > 44 ? substr($ad->title, 0, 45) . ' ...' : $ad->title;
                    echo $titleAds;
                    ?>
                </div> 
                <span class="location_box_ads">
                    <i class="glyphicon glyphicon-map-marker"></i>
                    <?= ucfirst($ad->location->seoname); ?>
                </span>                       
                <span class="price_box_ads"><?= html_entity_decode(i18n::money_format($ad->price)) ?></span>

            </div>
        </div>     
    <?endforeach?>
    
    <div class="container" style="float:left;margin-top:20px;">        
        <?=$pagination?>
    </div>
 <?elseif (count($ads) == 0):?>
 <!-- Case when we dont have ads for specific category / location -->
  <div class="page-header">
      <h3><?=__('We do not have any advertisements in this category')?></h3>
  </div>
<?endif?>
